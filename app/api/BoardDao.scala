package api

import play.api.libs.json.{Json, OFormat}

case class BoardDao (interests: Option[Seq[String]], title: Option[String], withCache: Boolean = false)

object BoardDao {
  implicit val boardFormat : OFormat[BoardDao]  = Json.format[BoardDao]
}