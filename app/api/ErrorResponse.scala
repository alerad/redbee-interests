package api

import play.api.libs.json.{Json, OFormat}

case class ErrorResponse (developerMessage: String)

object ErrorResponse {
  implicit val errorFormat : OFormat[ErrorResponse]  = Json.format[ErrorResponse]
}