package api

import play.api.libs.json.{Json, OFormat}

case class Interest (interest: String, max_id: Option[Long])


object Interest {
  implicit val interestsResponse :OFormat[Interest] = Json.format[Interest]
}
