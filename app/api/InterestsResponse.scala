package api

import play.api.libs.json.{Json, OFormat}
import services.{SimpleSearchMetadata, SimpleTweet}

case class InterestsResponse(tweets: Seq[SimpleTweet], metadata: Seq[SimpleSearchMetadata])

object InterestsResponse {
  implicit val interestsResponse :OFormat[InterestsResponse] = Json.format[InterestsResponse]
}