package controllers

import javax.inject.{Inject, Singleton}

import api.{BoardDao, ErrorResponse}
import com.danielasfregola.twitter4s.entities.{RatedData, StatusSearch}
import play.api.cache.AsyncCacheApi
import play.api.libs.json.Json

import scala.concurrent.ExecutionContext.Implicits.global
import play.api.mvc.{AbstractController, ControllerComponents}
import services.{TwitterService}

import scala.concurrent.Future

@Singleton
class InterestsController @Inject()(cache: AsyncCacheApi,  cc: ControllerComponents, twitterService: TwitterService) extends AbstractController(cc) {

  def post() = Action.async { request =>

    val interestsJson = request.body.asJson.get
    val board = interestsJson.as[BoardDao]


    board.interests match {
      case None => Future { BadRequest(Json.toJson(new ErrorResponse("Pedime intereses mono"))) }
      case Some(interests) => {
        val newInterests = twitterService.generateQueryString(interests, board.withCache)
        Future.sequence(newInterests).map(res=>res).flatMap{ interestsSeq=>
          twitterService.searchByInterests(interestsSeq).map { ratedData =>
            cacheMaxIds(ratedData)
            val concat= twitterService.concatInterests(ratedData)
            val simpleTweets = twitterService.generateSimpleTweets(concat)
            Ok(Json.toJson(simpleTweets))
          }
        }
      }
    }
  }


  //TODO, cache con usuario en base a jwt
  def cacheMaxIds(ratedData: Seq[RatedData[StatusSearch]]) = {
    ratedData.map { data=>
      val meta = data.data.search_metadata
      cache.set(meta.query.replaceAll("%2B.*","").replaceAll("%23","#").replaceAll("%40", "@"), meta.max_id)
    }
  }

}
