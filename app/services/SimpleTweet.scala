package services

import java.util.Date

import com.danielasfregola.twitter4s.entities.ExtendedTweet
import play.api.libs.json.{Json, OFormat}

case class SimpleTweet(created_at: Date,
                       extended_tweet: String,
                       id_str: String,
                       text: String,
                       media_url: Option[String]
                            )

object SimpleTweet {
  implicit val writesTweet = Json.writes[SimpleTweet]
  implicit val formatTweet : OFormat[SimpleTweet]= Json.format[SimpleTweet]
}


