package services
import javax.inject.Inject

import api.Interest
import com.danielasfregola.twitter4s.TwitterRestClient
import com.danielasfregola.twitter4s.entities.enums.TweetMode
import com.danielasfregola.twitter4s.entities._
import play.api.cache.AsyncCacheApi

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global



class TwitterService @Inject() (cache: AsyncCacheApi) {
  val restClient = TwitterRestClient()

  def searchByInterests(interests: Seq[Interest]) : Future[Seq[RatedData[StatusSearch]]] = {
    Future.traverse(interests)(interest => {
      restClient.searchTweet(interest.interest, tweet_mode = TweetMode.Extended, since_id =interest.max_id)
    })
  }

  def concatInterests(interests: Seq[RatedData[StatusSearch]]) = {
    interests.foldLeft(List[Tweet]()) {(acc: List[Tweet], interest: RatedData[StatusSearch])=> acc ++ interest.data.statuses }
  }

  def concatMetadata(metadata: Seq[RatedData[StatusSearch]], interest : String) = {
    metadata.foldLeft(List[SimpleSearchMetadata]()) {(acc: List[SimpleSearchMetadata], metadata: RatedData[StatusSearch])=> acc ++ Seq(new SimpleSearchMetadata(query = metadata.data.search_metadata.query, max_id = metadata.data.search_metadata.max_id))}
  }


  def createSimpleTweet(tweet :Tweet): SimpleTweet = {
    val extended = tweet.extended_tweet match {
      case None => ""
      case Some(extended_tweet) => extended_tweet.full_text
    }

    val media : Seq[Media] = tweet.entities match {
      case None => Nil
      case Some(entities) => entities.media
    }

    media match {
      case Nil => new SimpleTweet(tweet.created_at, extended, tweet.id_str, tweet.text, None)
      case _ => new SimpleTweet(tweet.created_at, extended, tweet.id_str, tweet.text, Some(media.head.media_url))
    }

  }

  def generateSimpleTweets(tweets: List[Tweet]): Seq[SimpleTweet] = {
    tweets.map { tweet =>
      createSimpleTweet(tweet)
    }
  }

  def generateQueryString(interests: Seq[String], withCache: Boolean) : Seq[Future[Interest]]= {
    val separator = "+-filter:retweets"
      interests.map({ interest =>
        if (withCache)
          cache.get[Long](interest).map {
            case None => new Interest(interest = interest + separator, None)
            case Some(maxid) => new Interest(interest = interest + separator, max_id = Some(maxid))
          }
        else
          Future{new Interest(interest = interest + separator, None)}
      })

  }

}
