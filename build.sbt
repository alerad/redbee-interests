name := "redbee-interests-service"

version := "1.0"

lazy val `redbee-interests-service` = (project in file(".")).enablePlugins(PlayScala)

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"

resolvers += "Akka Snapshot Repository" at "http://repo.akka.io/snapshots/"

scalaVersion := "2.12.2"

libraryDependencies ++= Seq( jdbc , ehcache , ws , specs2 % Test , guice )

unmanagedResourceDirectories in Test <+=  baseDirectory ( _ /"target/web/public/test" )

libraryDependencies += "com.danielasfregola" %% "twitter4s" % "5.5"

PlayKeys.playDefaultPort := 9002

libraryDependencies += "com.github.xuwei-k" %% "play-json-extra" % "0.5.1"

libraryDependencies ++= Seq(
  ehcache
)